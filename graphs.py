from sklearn import datasets
import matplotlib.pyplot as plt

iris = datasets.load_iris()
x = iris.data
y = iris.target

feature_names = ["Sepal length", "Sepal width", "Petal length", "Petal width"]
def plot_features(fx, fy, ax):
    x_min, x_max = x[:, fx].min() - 0.5, x[:, fx].max() + 0.5
    y_min, y_max = x[:, fy].min() - 0.5, x[:, fy].max() + 0.5

    scatter = ax.scatter(x[:, fx], x[:, fy], c=y, cmap=plt.cm.Set1, edgecolor="k")
    ax.set_xlabel(feature_names[fx])
    ax.set_ylabel(feature_names[fy])
    legend = ax.legend(*scatter.legend_elements(), loc="lower right", title="Class label")
    ax.add_artist(legend)
    ax.set_xlim(x_min, x_max)
    ax.set_ylim(y_min, y_max)


fig, axs = plt.subplots(2, 3)
plot_features(0, 1, axs[0][0])
plot_features(0, 2, axs[0][1])
plot_features(0, 3, axs[0][2])
plot_features(1, 2, axs[1][0])
plot_features(1, 3, axs[1][1])
plot_features(2, 3, axs[1][2])

plt.show()
