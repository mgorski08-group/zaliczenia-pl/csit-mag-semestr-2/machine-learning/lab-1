import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.linear_model import LogisticRegression
import numpy as np

iris = datasets.load_iris()
x = iris.data
y = iris.target

random0 = np.random.permutation(np.arange(0, 50))
random1 = np.random.permutation(np.arange(50, 100))
random2 = np.random.permutation(np.arange(100, 150))

x0 = x[random0[:40]]
x1 = x[random1[:40]]
x2 = x[random2[:40]]

y0 = y[random0[:40]]
y1 = y[random1[:40]]
y2 = y[random2[:40]]

x0_test = x[random0[40:]]
x1_test = x[random1[40:]]
x2_test = x[random2[40:]]

y0_test = y[random0[40:]]
y1_test = y[random1[40:]]
y2_test = y[random2[40:]]

x01_train = np.concatenate([x0[:, 0:2], x1[:, 0:2]])
y01_train = np.concatenate([y0, y1])
x01_test = np.concatenate([x0_test[:, 0:2], x1_test[:, 0:2]])
y01_test = np.concatenate([y0_test, y1_test])

clf = LogisticRegression(random_state=0).fit(x01_train, y01_train)

b = clf.intercept_[0]
w1, w2 = clf.coef_.T

c = -b/w2
m = -w1/w2

xmin, xmax = np.min(x01_train, 0)[0]-1, np.max(x01_train, 0)[0]+1
ymin, ymax = np.min(x01_train, 0)[1]-1, np.max(x01_train, 0)[1]+1

xd = np.array([xmin, xmax])
yd = m*xd + c

plt.figure()
plt.plot(xd, yd, 'k', lw=1, ls='--')
plt.fill_between(xd, yd, ymin, color='tab:blue', alpha=0.2)
plt.fill_between(xd, yd, ymax, color='tab:orange', alpha=0.2)

plt.scatter(*x01_train.T, c=y01_train, cmap=plt.cm.Set1, edgecolors='k')
plt.scatter(*x01_test.T, c=y01_test, cmap=plt.cm.Set1, edgecolors='b')

plt.xlim(xmin, xmax)
plt.ylim(ymin, ymax)
plt.ylabel("Sepal width")
plt.xlabel("Sepal length")

plt.show()
